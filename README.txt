CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Note


INTRODUCTION
------------

This module uses the Instagram Graph API to link your Instagram Business account
with your professional Facebook Page and link your Instagram account to your
Drupal site.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/instagram_graph

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/instagram_graph


REQUIREMENTS
------------

This module requires the following library:

 * Facebook Graph SDK for PHP (https://github.com/facebook/php-graph-sdk)


INSTALLATION
------------

 * Install as you would normally install a Drupal theme. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.


CONFIGURATION
-------------

 * This module uses the new Instagram Graph API
   https://developers.facebook.com/docs/instagram-api/ .

 * Create a Facebook application and link your Instagram Business account to it.
   Link your Instagram Business account with your professional Facebook Page.

 * Configure the Social media by adding your App Id and App secret from
   /admin/config/social-media/instagram


MAINTAINERS
-----------

Current maintainers:
 * Francis Santerre (santerref) - https://www.drupal.org/u/santerref


NOTE
----

Install this module using composer so you don't have to install Facebook
Graph SDK for PHP api manually.
