<?php

namespace Drupal\instagram_graph\Plugin\Block;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\instagram_graph\Facebook;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base block implementation.
 *
 * @Block(
 *   id = "instagram_graph_media",
 *   admin_label = @Translation("Instagram Media"),
 *   category = @Translation("Instagram"),
 * )
 */
class MediaBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The facebook service.
   *
   * @var \Drupal\instagram_graph\Facebook
   */
  protected $facebookService;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Facebook $facebook, TimeInterface $time, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->facebookService = $facebook;
    $this->time = $time;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('instagram_graph.facebook'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $this->getImageStylesOptions(),
      '#default_value' => isset($config['image_style']) ? $config['image_style'] : NULL,
    ];

    $form['images_count'] = [
      '#title' => $this->t('Image count'),
      '#type' => 'number',
      '#min' => 1,
      '#step' => 1,
      '#size' => 5,
      '#default_value' => isset($config['images_count']) ? $config['images_count'] : 12,
      '#required' => TRUE,
    ];

    $form['show_likes_count'] = [
      '#title' => $this->t('Show likes count'),
      '#type' => 'checkbox',
      '#default_value' => isset($config['show_likes_count']) ? $config['show_likes_count'] : FALSE,
    ];

    $form['show_date'] = [
      '#title' => $this->t("Show the media's publish date"),
      '#type' => 'checkbox',
      '#default_value' => isset($config['show_date']) ? $config['show_date'] : FALSE,
    ];

    $form['expire'] = [
      '#title' => $this->t('Cache duration'),
      '#description' => $this->t('Leave empty to disable cache or enter a positive number in minutes (0 means permanent cache).'),
      '#type' => 'number',
      '#min' => 0,
      '#step' => 1,
      '#size' => 5,
      '#default_value' => isset($config['expire']) ? $config['expire'] : 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('image_style', $form_state->getValue('image_style'));
    $this->setConfigurationValue('images_count', $form_state->getValue('images_count'));
    $this->setConfigurationValue('show_likes_count', $form_state->getValue('show_likes_count'));
    $this->setConfigurationValue('show_date', $form_state->getValue('show_date'));
    $this->setConfigurationValue('expire', $form_state->getValue('expire'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $expire = !is_numeric($config['expire']) ? Cache::PERMANENT : $config['expire'];
    $expire_timestamp = $expire + $this->time->getCurrentTime();
    $cache_images = ($expire > 0);

    $media = $this->facebookService->getInstagramImages(
      $config['images_count'],
      $cache_images,
      $expire_timestamp
    );

    return [
      '#theme' => 'instagram_media',
      '#media' => $media,
      '#show_likes_count' => isset($config['show_likes_count']) ? $config['show_likes_count'] : FALSE,
      '#show_date' => isset($config['show_date']) ? $config['show_date'] : FALSE,
      '#image_style' => $config['image_style'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    $config = $this->getConfiguration();
    $max_age = 0;
    if (isset($config['expire']) && is_numeric($config['expire'])) {
      if ($config['expire'] > 0) {
        $max_age = $config['expire'];
      }
      else {
        $max_age = Cache::PERMANENT;
      }
    }

    return $max_age;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = parent::getCacheTags();

    $tags[] = 'facebook_instagram';
    $tags[] = 'instagram_images';

    return $tags;
  }

  /**
   * Build the options array for the images_style block config.
   *
   * @return array
   *   The available options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getImageStylesOptions() {
    $options = [];
    $image_styles = $this->entityTypeManager->getStorage('image_style')->loadMultiple();
    foreach ($image_styles as $id => $image_style) {
      $options[$id] = $image_style->label();
    }
    return $options;
  }

}
