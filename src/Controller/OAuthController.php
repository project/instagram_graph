<?php

namespace Drupal\instagram_graph\Controller;

use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\instagram_graph\Facebook;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Handle OAuth callback from Facebook.
 */
class OAuthController extends ControllerBase {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The facebook service.
   *
   * @var \Drupal\instagram_graph\Facebook
   */
  protected $facebookService;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The cache tags invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   */
  protected $cacheTagsInvalidator;

  /**
   * OAuthController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\instagram_graph\Facebook $facebook
   *   The facebook service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidator $cacheTagsInvalidator
   *   The cache tags invalidator service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Facebook $facebook, CacheTagsInvalidator $cacheTagsInvalidator, MessengerInterface $messenger) {
    $this->config = $configFactory->getEditable('instagram_graph.access_tokens');
    $this->facebookService = $facebook;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('instagram_graph.facebook'),
      $container->get('cache_tags.invalidator'),
      $container->get('messenger')
    );
  }

  /**
   * Try to get a long lived access token.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect user to the settings page.
   */
  public function auth() {
    if ($accessToken = $this->facebookService->getLongLivedAccessToken()) {
      $this->config->set('facebook_page', NULL);
      $this->config->set('instagram_account', NULL);
      $this->cacheTagsInvalidator->invalidateTags(['facebook_instagram']);

      $this->config->set('access_token', $accessToken)->save();

      $this->messenger->addMessage($this->t('Your account is successfully linked to your website.'));
    }
    else {
      $this->config->clear('access_token')
        ->clear('user_id')
        ->save();
      $this->messenger->addError($this->t('We were unable to retrieve the long-lived access token.'));
    }

    return $this->redirect('instagram_graph.settings');
  }

}
