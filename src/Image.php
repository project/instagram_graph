<?php

namespace Drupal\instagram_graph;

use Drupal\Component\Utility\Random;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * The image service.
 */
class Image {

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Image constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The filesystem service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityManager service.
   */
  public function __construct(FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager) {
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Download an external image in files.
   *
   * @param string $url
   *   The URL to download.
   * @param null|string $image_style
   *   The image style to apply to the downloaded image.
   *
   * @return bool|\Drupal\Core\GeneratedUrl|string
   *   The URL to the image.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function external($url, $image_style = NULL) {
    $imageUrl = FALSE;
    $filename = $this->getFilenameNameFromUrl($url);
    if (!file_exists('public://instagram')) {
      $this->fileSystem->mkdir('public://instagram');
    }
    $uri = 'public://instagram/' . $filename;
    if (!file_exists($uri)) {
      $uri = system_retrieve_file($url, $uri, FALSE, FileSystemInterface::EXISTS_REPLACE);
    }

    if (file_exists($uri)) {
      $imageUrl = Url::fromUri(file_create_url($uri), ['absolute' => TRUE])->toString();

      if ($image_style) {
        $style_entity = $this->entityTypeManager->getStorage('image_style')->load($image_style);
        if ($style_entity) {
          $imageUrl = $style_entity->buildUrl($uri);
        }
      }
    }

    return $imageUrl;
  }

  /**
   * Generate the image name to be stored based on the Instagram URL.
   *
   * @param string $url
   *   Instagram image URL.
   * @param bool $uniqueFallback
   *   Should we generate a random name no filename is found?
   *
   * @return mixed|string|null
   *   The filename or NULL if not found.
   */
  protected function getFilenameNameFromUrl($url, $uniqueFallback = TRUE) {
    $filename = NULL;
    $parsedUrl = parse_url($url);
    $pathParts = explode('/', $parsedUrl['path']);

    if (!empty($pathParts)) {
      $filename = end($pathParts);
    }

    if (empty($filename) && $uniqueFallback) {
      $random = new Random();
      $filename = $random->name(16, TRUE) . '.jpg';
    }

    return $filename;
  }

}
