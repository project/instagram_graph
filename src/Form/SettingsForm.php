<?php

namespace Drupal\instagram_graph\Form;

use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\instagram_graph\Facebook;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Module settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The facebook service.
   *
   * @var \Drupal\instagram_graph\Facebook
   */
  protected $facebookService;

  /**
   * The cache tags invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\instagram_graph\Facebook $facebook
   *   The facebook service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidator $cacheTagsInvalidator
   *   The cache tags invalidator service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Facebook $facebook, CacheTagsInvalidator $cacheTagsInvalidator) {
    parent::__construct($config_factory);
    $this->facebookService = $facebook;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('instagram_graph.facebook'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['instagram_graph.access_tokens'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_access_tokens';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('instagram_graph.access_tokens');

    $form['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('app_id'),
    ];

    $form['app_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('App Secret'),
      '#description' => $config->get('app_secret') ? $this->t('Leave blank to keep previous value.') : '',
    ];

    if (!$config->get('app_secret')) {
      $form['app_secret']['#required'] = TRUE;
    }

    if ($config->get('app_id') && $config->get('app_secret')) {
      if (!$config->get('access_token')) {
        $form['authenticate'] = [
          '#type' => 'link',
          '#title' => $this->t('Authenticate your Facebook account'),
          '#url' => Url::fromUri($this->facebookService->getLoginUrl(
            Url::fromRoute('instagram_graph.oauth', [], ['absolute' => TRUE])->toString(),
            ['instagram_basic', 'pages_show_list']
          )),
        ];
      }
      else {
        $facebookPages = $this->facebookService->getUserPages();

        $form['facebook_instagram_container'] = [
          '#type' => 'container',
          '#attributes' => [
            'id' => 'facebook-instagram-container',
          ],
        ];
        $facebookPageId = !empty($form_state->getValue('facebook_page')) ? $form_state->getValue('facebook_page') : $config->get('facebook_page');
        $form['facebook_instagram_container']['facebook_page'] = [
          '#type' => 'select',
          '#options' => $facebookPages,
          '#title' => $this->t('Facebook page'),
          '#required' => !empty($facebookPages),
          '#ajax' => [
            'callback' => '::instagramBusinessAccountAjax',
            'wrapper' => 'facebook-instagram-container',
          ],
          '#default_value' => $facebookPageId,
        ];

        $instagram_account = [];
        if ($facebookPageId) {
          $instagram_account = $this->facebookService->getPageInstagramBusinessAccount($facebookPageId);
        }
        $form['facebook_instagram_container']['instagram_account'] = [
          '#type' => 'select',
          '#options' => $instagram_account,
          '#title' => $this->t('Instagram business account'),
          '#required' => !empty($instagram_account),
          '#default_value' => $config->get('instagram_account'),
        ];
      }
    }

    if ($config->get('access_token')) {
      $form['actions']['relogin'] = [
        '#type' => 'link',
        '#title' => $this->t('Re-login with Facebook'),
        '#attributes' => [
          'class' => ['button'],
        ],
        '#url' => Url::fromUri($this->facebookService->getLoginUrl(
          Url::fromRoute('instagram_graph.oauth', [], ['absolute' => TRUE])->toString(),
          ['instagram_basic', 'pages_show_list']
        )),
      ];

      $form['actions']['logout'] = [
        '#type' => 'submit',
        '#value' => $this->t('Logout'),
        '#name' => 'logout',
        '#attributes' => [
          'style' => 'margin-left: 0;',
        ],
      ];
    }

    return $form;
  }

  /**
   * Handle facebook page dropdown ajax request.
   */
  public function instagramBusinessAccountAjax(array &$form, FormStateInterface $form_state) {
    return $form['facebook_instagram_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('instagram_graph.access_tokens');
    $triggeringElement = $form_state->getTriggeringElement();

    $this->cacheTagsInvalidator->invalidateTags(['instagram_images']);

    if ($triggeringElement && $triggeringElement['#name'] == 'logout') {
      $this->cacheTagsInvalidator->invalidateTags(['facebook_instagram']);
      $config->set('access_token', NULL);
      $config->set('facebook_page', NULL);
      $config->set('instagram_account', NULL);
      $this->messenger()->addStatus($this->t('You have been successfully logged out.'));
    }
    else {
      $appSecret = $form_state->getValue('app_secret');
      $config->set('facebook_page', $form_state->getValue('facebook_page'))
        ->set('instagram_account', $form_state->getValue('instagram_account'));

      if ($form_state->getValue('app_id') != $config->get('app_id')
          || (!empty($appSecret) && $appSecret != $config->get('app_secret'))
      ) {
        $config->set('access_token', NULL);
        $config->set('facebook_page', NULL);
        $config->set('instagram_account', NULL);
      }

      $config->set('app_id', $form_state->getValue('app_id'));
      if (!empty($appSecret)) {
        $config->set('app_secret', $appSecret);
      }

      parent::submitForm($form, $form_state);
    }
    $config->save();
  }

}
