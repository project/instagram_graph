<?php

namespace Drupal\instagram_graph;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook as FacebookSdk;

/**
 * The facebook service.
 */
class Facebook {

  /**
   * The module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Facebook SDK library instance.
   *
   * @var \Facebook\Facebook
   */
  protected $facebook;

  /**
   * The logger instance to log errors.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The default cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Facebook constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The default cache service.
   *
   * @throws \Facebook\Exceptions\FacebookSDKException
   */
  public function __construct(ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $loggerFactory, CacheBackendInterface $cache) {
    $this->config = $configFactory->get('instagram_graph.access_tokens');
    try {
      $this->facebook = new FacebookSdk(
        [
          'app_id' => $this->config->get('app_id'),
          'app_secret' => $this->config->get('app_secret'),
          'default_graph_version' => 'v3.3',
        ]
      );
    }
    catch (FacebookSDKException $e) {
      $this->facebook = NULL;
    }
    $this->logger = $loggerFactory->get('instagram_graph');
    $this->cache = $cache;
  }

  /**
   * Returns a URL for OAuth authentication with Facebook (with permissions).
   *
   * @param string $redirectUrl
   *   The URL to redirect the user to.
   * @param array $permissions
   *   The permissions to ask to the user.
   *
   * @return string
   *   The URL.
   */
  public function getLoginUrl($redirectUrl, array $permissions) {
    $url = '';

    if ($this->facebook) {
      $url = $this->facebook->getRedirectLoginHelper()->getLoginUrl($redirectUrl, $permissions);
    }

    return $url;
  }

  /**
   * Generate a long-lived access after a successful authorization.
   *
   * @return string|null
   *   The long-lived access token if available, null otherwise.
   */
  public function getLongLivedAccessToken() {
    $accessToken = NULL;

    if ($this->facebook) {
      try {
        $accessToken = $this->facebook->getRedirectLoginHelper()->getAccessToken();

        if ($accessToken) {
          if (!$accessToken->isLongLived()) {
            $accessToken = $this->facebook->getOAuth2Client()->getLongLivedAccessToken($accessToken);
          }
          $accessToken = $accessToken->getValue();
        }
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }

    return $accessToken;
  }

  /**
   * Let you retrieve a Facebook user's pages.
   *
   * @param string $userId
   *   The Facebook user ID or "me" for the current user.
   *
   * @return array
   *   The pages managed by the user.
   */
  public function getUserPages($userId = 'me') {
    if ($cache = $this->cache->get('facebook.' . $userId . '.pages')) {
      return $cache->data;
    }
    $pages = [];

    if ($this->facebook) {
      try {
        $response = $this->facebook->get('/' . $userId . '/accounts?summary=total_count&limit=35', $this->config->get('access_token'));
        $accountsGraphNodes = $response->getGraphEdge();
        $accounts = [];

        do {
          $accounts = array_merge($accounts, $accountsGraphNodes->asArray());
        } while ($accountsGraphNodes = $this->facebook->next($accountsGraphNodes));

        foreach ($accounts as $page) {
          $pages[$page['id']] = $page['name'];
        }

        $this->cache->set('facebook.' . $userId . '.pages', $pages, Cache::PERMANENT, ['facebook_instagram']);
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }

    return $pages;
  }

  /**
   * Retrieve the Instagram Business account associated to a Facebook page.
   *
   * @param mixed $pageId
   *   The Facebook page ID.
   *
   * @return array
   *   The list of Instagram Business accounts associated to the page.
   */
  public function getPageInstagramBusinessAccount($pageId) {
    if ($cache = $this->cache->get('facebook.page.' . $pageId . '.instagram_business_account')) {
      return $cache->data;
    }
    $instagramBusinessAccount = [];

    if ($this->facebook) {
      try {
        $response = $this->facebook->get('/' . $pageId . '?fields=instagram_business_account', $this->config->get('access_token'));
        $decodedBody = $response->getDecodedBody();
        if (isset($decodedBody['instagram_business_account'])) {
          $instagramBusinessAccountId = $decodedBody['instagram_business_account']['id'];
          $response = $this->facebook->get('/' . $instagramBusinessAccountId . '?fields=username,name', $this->config->get('access_token'));
          $decodedBody = $response->getDecodedBody();
          $instagramBusinessAccount[$instagramBusinessAccountId] = $decodedBody['name'] . ' (@' . $decodedBody['username'] . ')';

          $this->cache->set('facebook.page.' . $pageId . '.instagram_business_account', $instagramBusinessAccount, Cache::PERMANENT, ['facebook_instagram']);
        }
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }

    return $instagramBusinessAccount;
  }

  /**
   * Load images from the Instagram Business account.
   *
   * @param int $limit
   *   Number of images to retreive (maximum).
   * @param bool $cache_images
   *   Should we store the results in cache?
   * @param int $expire
   *   A Unix timestamp that the item will be invalid after this time.
   *
   * @return array
   *   The images array with raw result from Graph API.
   */
  public function getInstagramImages($limit = 12, $cache_images = TRUE, $expire = Cache::PERMANENT) {
    $instagramBusinessAccountId = $this->config->get('instagram_account');
    if ($cache_images) {
      if ($cache = $this->cache->get('instagram.' . $instagramBusinessAccountId . '.media.limit.' . $limit)) {
        return $cache->data;
      }
    }
    $images = [];

    if ($this->facebook) {
      try {
        $response = $this->facebook->get('/' . $instagramBusinessAccountId . '/media?fields=caption,timestamp,like_count,media_type,media_url,permalink&limit=' . $limit, $this->config->get('access_token'));
        $mediaGraphNodes = $response->getGraphEdge();
        $media = [];

        do {
          $filteredMedia = array_filter(
            $mediaGraphNodes->asArray(),
            function ($media) {
              return in_array($media['media_type'], ['IMAGE', 'CAROUSEL_ALBUM']);
            }
          );
          $media = array_merge($media, $filteredMedia);
          if (count($media) >= $limit) {
            break;
          }
        } while ($mediaGraphNodes = $this->facebook->next($mediaGraphNodes));

        $media = array_slice($media, 0, $limit);
        foreach ($media as &$item) {
          $item['timestamp'] = strtotime($item['timestamp']);
        }
        $timestamps = array_column($media, 'timestamp');
        array_multisort($timestamps, SORT_DESC, $media);
        foreach ($media as $image) {
          $images[$image['id']] = $image;
        }

        if ($cache_images) {
          $this->cache->set('instagram.' . $instagramBusinessAccountId . '.media.limit.' . $limit, $images, $expire, ['facebook_instagram', 'instagram_images']);
        }
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }

    return $images;
  }

  /**
   * Current Instagram Business account username.
   *
   * @return mixed|null
   *   The username if available.
   */
  public function getCurrentUsername() {
    if ($cache = $this->cache->get('instagram.username')) {
      return $cache->data;
    }
    $username = NULL;
    $instagramBusinessAccountId = $this->config->get('instagram_account');

    if (!empty($instagramBusinessAccountId) && $this->facebook) {
      try {
        $response = $this->facebook->get('/' . $instagramBusinessAccountId . '?fields=username,name', $this->config->get('access_token'));
        $decodedBody = $response->getDecodedBody();
        $username = $decodedBody['username'];

        $this->cache->set('instagram.username', $username, Cache::PERMANENT, ['facebook_instagram']);
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }

    return $username;
  }

}
